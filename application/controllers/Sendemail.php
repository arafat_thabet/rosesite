<?php defined('BASEPATH') or exit('No direct script access allowed');

class Sendemail extends CI_Controller
{

	    /**  __construct function  */
    // --------------------------------------------------------------------------------------------------
    public function __construct()
    {
        parent::__construct();
        $this->load->model('About_model', 'aboutModel');
	}
	///////////////////////// AJAX Mail /////////////
	
	public function sendemail()
	{
		// bringing the reciever name and email        
		$data = $this->aboutModel->get_about();
		$toEmail = $data->email;
		
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$message = $this->input->post('message');
		
		$mailHeaders = "From: ".$email . "\r\n";
		
		if(mail($toEmail, $name, $message, $mailHeaders)) {
		echo "<p class='success'>Thank you, your message has been sent successfully.</p>";
		
		} else {
		echo "<p class='Error'>Sorry, problem in Sending Mail.</p>";
		}
	}
}
