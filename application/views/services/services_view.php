<?php if (null !== $this->session->flashdata('success_add')): ?>
	<div class="alert alert-success  offset-md-2 col-md-10" role="alert">
		<?php echo $this->session->flashdata('success_add'); ?>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</div>
<?php elseif (null !== $this->session->flashdata('error_add')): ?>
	<div class="alert alert-warning  offset-md-2 col-md-10" role="alert">
		<?php echo $this->session->flashdata('error_add'); ?>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</div>
<?php elseif (null !== $this->session->flashdata('updated')): ?>
	<div class="alert alert-success  offset-md-2 col-md-10" role="alert">
	<?php echo $this->session->flashdata('updated'); ?>
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	<span aria-hidden="true">&times;</span>
	</div>
<?php elseif (null !== $this->session->flashdata('deleted')): ?>
	<div class="alert alert-danger  offset-md-2 col-md-10" role="alert">
	<?php echo $this->session->flashdata('deleted'); ?>
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	<span aria-hidden="true">&times;</span>
	</div>
<?php endif;?>

<h2 class="text-center py-3">Services </h2>
<a href='<?php echo base_url('services/add_service') ?>' class="btn btn-primary mb-2">Add new</a>
<small>You must have only <big>4</big> services</small>

<table class="table table-dark text-center">
<thead>
	<tr>
		<th scope="col">Title</th>
		<th scope="col">Number</th>
		<th scope="col">Order</th>
	</tr>
</thead>
<tbody>
	<?php foreach($services as $serv) :?>
	<tr>
	<td><?= $serv->title; ?></td>
	<td><?= $serv->number; ?></td>
	<td><?= $serv->order; ?></td>
	<td>
		<a href="<?php echo base_url().'services/edit_service/'. $serv->id?>" class="btn btn-success">Edit</a>
		<a href="<?php echo base_url().'services/delete_service/'. $serv->id ?>" class="btn btn-danger">Delete</a>
	</td>
	</tr>
	<?php endforeach; ?>
</tbody>
</table>
